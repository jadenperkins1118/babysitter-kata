package perkins.jaden;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Tests {
    @Test
    public void payRateAfterMidnightIsAlwaysMidnightRate() {
        for (int hour = 7; hour <= 11; hour++) {
            for (int bedTime = 0; bedTime <= 11; bedTime++) {
                assertEquals(Main.MIDNIGHT_RATE, Main.getPayRate(hour, bedTime));
            }
        }
    }

    @Test
    public void payRateBeforeBedtimeIsPreBedtimeRate() {
        for (int bedTime = 1; bedTime < 7; bedTime++) {
            for (int hour = 0; hour < bedTime; hour++) {
                assertEquals(Main.PRE_BEDTIME_RATE, Main.getPayRate(hour, bedTime));
            }
        }
    }

    @Test
    public void payRateBetweenBedtimeAndMidnightIsPostBedtimeRate() {
        for (int bedTime = 0; bedTime < 7; bedTime++) {
            for (int hour = bedTime; hour < 7; hour++) {
                assertEquals(Main.POST_BEDTIME_RATE, Main.getPayRate(hour, bedTime));
            }
        }
    }

    @Test
    public void midnightBedtimeIsMaximumPayment() {
        assertEquals(148, Main.getPay(0, 11, 7));
    }
}
