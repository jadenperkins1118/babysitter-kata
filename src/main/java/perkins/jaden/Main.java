package perkins.jaden;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.function.Predicate;

public class Main {
    private static final SimpleHour START_HOUR = new SimpleHour(5, "PM");
    private static final SimpleHour END_HOUR = new SimpleHour(4, "AM");

    public static void main(String[] args) {
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        int startTime = getInput(input, "start time");
        int endTime = getInputWithValidation(input, "end time", "End time must be after start time", it -> it > startTime);
        int bedTime = getInputWithValidation(input, "bed time", "Bed time must be between start and end times", it -> it >= startTime && it <= endTime);

        //calculate and display the pay based on the inputs
        int pay = getPay(startTime, endTime, bedTime);
        System.out.println("Pay for the shift is $" + pay);
    }

    public static int getPay(int start, int end, int bed) {
        int total = 0;
        //for each hour worked, add that hour's pay rate to the total
        for (int hour = 0; hour < (end - start); hour++) {
            total += getPayRate(start + hour, bed);
        }
        return total;
    }

    //constants for pay rates
    public static final int PRE_BEDTIME_RATE = 12;
    public static final int POST_BEDTIME_RATE = 8;
    public static final int MIDNIGHT_RATE = 16;

    public static int getPayRate(int hour, int bedTime) {
        //anytime after midnight is always the midnight pay rate
        if (hour >= 12 - START_HOUR.getHour()) return MIDNIGHT_RATE;
        //before bedtime? pre-bedtime rate, otherwise post-bedtime rate
        return hour < bedTime ? PRE_BEDTIME_RATE : POST_BEDTIME_RATE;
    }

    /**Repeatedly attempt to get an int input until it is in the correct format*/
    private static int getInput(BufferedReader reader, String inputName) {
        while (true) {
            System.out.printf("Enter an hour between %s and %s for %s; type exit to quit%n", START_HOUR.getDisplay(), END_HOUR.getDisplay(), inputName);
            try {
                String result = reader.readLine();
                if (Objects.equals(result, "exit")) System.exit(0); //exit the program

                int parsed = Integer.parseInt(result); //throws an exception if it isn't a number
                if (parsed < 1 || parsed > 12) continue; //do another pass

                int a = parsed - START_HOUR.getHour();
                if (a < 0) return a + 12; //wrap numbers to [0, 12)
                return a;
            } catch (IOException | NumberFormatException e) {
                System.out.println("Invalid input; try again");
            }
        }
    }

    /**Repeatedly get an input that is in the correct format and passes a validation check*/
    private static int getInputWithValidation(BufferedReader reader, String inputName, String failure, Predicate<Integer> validator) {
        int ret = getInput(reader, inputName);
        while (!validator.test(ret)) {
            System.out.println(failure + "; try again");
            ret = getInput(reader, inputName);
        }
        return ret;
    }
}
