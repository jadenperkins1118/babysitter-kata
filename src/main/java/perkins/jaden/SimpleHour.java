package perkins.jaden;

public class SimpleHour {
    private final int hour;
    private final String marker;

    public SimpleHour(int hour, String marker) {
        this.hour = hour;
        this.marker = marker;
    }

    public int getHour() {
        return hour;
    }
    public String getDisplay() {
        return hour + " " + marker;
    }
}
